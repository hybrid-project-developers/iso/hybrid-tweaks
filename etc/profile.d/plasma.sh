#!/usr/bin/env bash

if [[ "$XDG_CURRENT_DESKTOP" == "KDE" ]]; then
  # Fix GTK scroll
  export GDK_CORE_DEVICE_EVENTS=1
  #Used to allow maliit virtual keyboard to adapt its colors to the current theme
  export QT_QUICK_CONTROLS_STYLE=org.kde.desktop
fi

#!/usr/bin/env bash

# Add .local/bin to PATH
export PATH=~/.local/bin:$PATH

# Uncomment the line below if you get the error "No Space Left" when using pip install
#export TMPDIR='/var/tmp'

# Set $EDITOR to nano if is empty
if [ -z "$EDITOR" ]; then
  export EDITOR=micro
fi

# Allow npm to run not from sudo (For security)
if command -v npm &>/dev/null; then
  npm config set prefix ~/.local
fi

# Pixel-perfect trackpad scrolling
export MOZ_USE_XINPUT2=1

# GPG_TTY needs to be set to the value in tty in order for GPG to work properly
export GPG_TTY="$(tty)"

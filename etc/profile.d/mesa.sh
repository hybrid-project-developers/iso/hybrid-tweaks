#!/usr/bin/env bash

# Check for the Intel i915 module
if [[ -d /sys/module/i915 ]]; then
  # Enable Graphics Pipeline Library support on Intel ANV Vulkan driver
  export ANV_GPL=true
fi

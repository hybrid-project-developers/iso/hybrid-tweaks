#!/usr/bin/env bash

if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    export WINIT_UNIX_BACKEND=x11
    export MOZ_ENABLE_WAYLAND=1
	export MOZ_DBUS_REMOTE=1
    export WLR_NO_HARDWARE_CURSORS=1
    export KITTY_ENABLE_WAYLAND=1
	export _JAVA_AWT_WM_NONREPARENTING=1
	export BEMENU_BACKEND=wayland
	export CLUTTER_BACKEND=wayland
fi

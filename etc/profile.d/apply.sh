#!/usr/bin/env bash

if [ "${SHELL}" != "/bin/bash" ]; then
    for file in /etc/profile.d/*.sh; do
        if [ "$(basename "$file")" != "apply.sh" ]; then
            . "$file"
        fi
    done
fi

#!/usr/bin/env bash

if [[ -d /sys/module/nvidia ]]; then
    # Common settings for Nvidia
    export __GL_YIELD="USLEEP"
    export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
    export VDPAU_DRIVER=nvidia
    export LIBVA_DRIVER_NAME=nvidia
    export NVD_BACKEND=direct
    export MOZ_DISABLE_RDD_SANDBOX=1
fi

if [[ -d /sys/module/nouveau ]]; then
    export VDPAU_DRIVER=nouveau
    export LIBVA_DRIVER_NAME=nouveau
fi

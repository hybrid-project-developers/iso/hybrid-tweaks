#!/usr/bin/env bash

if [ "$(id -u)" -ne 0 ]; then
  echo "Error: This script must be run with root privileges."
  exit 1
fi

ram_size=$(free -g | awk '/^Mem:/ {print $2}')

vm=$(systemd-detect-virt)

# Check if SSD, HDD, or NVMe
if lsblk -d -o rota | grep -q '^0$'; then
  storage_type="ssd"
elif lsblk -d -o tran | grep -q 'nvme'; then
  storage_type="nvme"
else
  storage_type="hdd"
fi

if swapon --show | grep -q "zram"; then
  sysctl -w vm.page-cluster=0
  sysctl -w vm.swappiness=150
else
  sysctl -w vm.swappiness=100
fi

if [[ $storage_type == "ssd" ||  $storage_type == "nvme" ]]; then
  sysctl -w vm.vfs_cache_pressure=50
  sysctl -w vm.page-cluster=0
elif [[ $ram_size -gt 1 && $storage_type == "hdd" ]]; then
  sysctl -w vm.vfs_cache_pressure=1000
fi

if [ "$vm" == "vmware" ]; then
    sysctl -w vm.compaction_proactiveness=0
fi

if [[ -d /sys/module/i915 ]]; then
  sysctl -w dev.i915.perf_stream_paranoid=0
fi

if [[ -d /sys/module/nvidia ]]; then
  sysctl -w vm.panic_on_oom=0
fi

# Set CPU governor to ondemand if available
if [[ -e "/sys/devices/system/cpu/cpu*/cpufreq/scaling_available_governors" ]] && \
   grep -q 'ondemand' /sys/devices/system/cpu/cpu*/cpufreq/scaling_available_governors; then
  for cpu in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do
    echo "ondemand" | tee "$cpu"
  done
  echo "60" | tee /sys/devices/system/cpu/cpufreq/ondemand/up_threshold
  echo "55" | tee /sys/devices/system/cpu/cpufreq/ondemand/down_threshold
  echo "5" | tee /sys/devices/system/cpu/cpufreq/ondemand/sampling_down_factor
fi
